﻿portrait_kheteratan_clothing_modifier = {
	modifier = {
		add = 50
        portrait_kheteratan_clothing_trigger = yes
	}
}

portrait_kheteratan_beards_clothes_modifier = {
	modifier = {
		add = 70
        portrait_kheteratan_clothing_trigger = yes
	}
}
