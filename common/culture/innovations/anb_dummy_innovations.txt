﻿# Used to create dynamic textures, these are never used in game by normal players (LET THERE BE JANK!)

@munas = "gfx/interface/icons/faith/munas.dds"
@dame = "gfx/interface/icons/faith/the_dame.dds"
patron_munas = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @munas

	potential = {
		always = no
	}
}
patron_the_dame = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_castellos = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_esmaryal = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_falah = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_nerat = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_adean = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_ryala = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_balgar = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_ara = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_minara = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_nathalyne = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_begga = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_uelos = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}
}
patron_wip = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
##
patron_ariadas = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_histra = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_wulkas = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_ragna = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_treyana = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_hyntran = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_welas = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_rendan = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
##
patron_artanos = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_merisse = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
 patron_trovecos = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
 patron_careslobos = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
 patron_asmirethin = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_damarta = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_belouina = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_dercanos = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_turanos = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}
patron_sorbodua = {
	culture_era = culture_era_early_medieval
	group =  culture_group_regional
	icon = @dame

	potential = {
		always = no
	}	
}