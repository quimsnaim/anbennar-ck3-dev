﻿skaldhyrric_religion = {
	family = rf_skaldhyrric
	graphical_faith = pagan_gfx
	doctrine = skaldhyrric_hostility_doctrine 
	
	pagan_roots = yes

	#Main Group
	doctrine = doctrine_spiritual_head	
	doctrine = doctrine_gender_equal
	doctrine = doctrine_pluralism_pluralistic
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_monogamy
	doctrine = doctrine_divorce_disallowed
	doctrine = doctrine_bastardry_none
	doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece

	#Crimes
	doctrine = doctrine_homosexuality_crime
	doctrine = doctrine_adultery_men_crime
	doctrine = doctrine_adultery_women_crime
	doctrine = doctrine_kinslaying_any_dynasty_member_crime
	doctrine = doctrine_deviancy_crime
	doctrine = doctrine_witchcraft_crime

	#Clerical Functions
	doctrine = doctrine_clerical_function_alms_and_pacification
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	
	#Allow pilgrimages
	doctrine = doctrine_pilgrimage_encouraged
	
	#Funeral tradition
	doctrine = doctrine_funeral_stoic # placeholder
	
	traits = {	#standard castanorian
		virtues = {
			brave
			gregarious
			one_legged
		}
		sins = {
			craven
			shy
			lazy
		}
	}

	custom_faith_icons = {	#TODO
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = {
		{ name = "holy_order_jomsvikings" }
		{ name = "holy_order_faithful_of_tyr" }
		{ name = "holy_order_odins_valkyries" }
		{ name = "holy_order_chosen_of_freyja" }
	}

	holy_order_maa = { jomsviking_pirates }

	localization = { #TODO - Come up with something for this
		HighGodName = skaldhyrric_high_god_name
		HighGodName2 = skaldhyrric_high_god_name
		HighGodNamePossessive = skaldhyrric_high_god_name_possessive
		HighGodNameSheHe = skaldhyrric_high_god_shehe
		HighGodHerselfHimself = skaldhyrric_high_god_herselfhimself
		HighGodHerHis = skaldhyrric_high_god_herhis
		HighGodNameAlternate = skaldhyrric_high_god_name_alternate
		HighGodNameAlternatePossessive = skaldhyrric_high_god_name_alternate_possessive
		
		#Creator
		CreatorName = paganism_creator_god_name
		CreatorNamePossessive = paganism_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_SHE
		CreatorHerHis = CHARACTER_HERHIS_HER
		CreatorHerHim = CHARACTER_HERHIM_HER

		#HealthGod
		HealthGodName = paganism_health_god_name
		HealthGodNamePossessive = paganism_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_SHE
		HealthGodHerHis = CHARACTER_HERHIS_HER
		HealthGodHerHim = CHARACTER_HERHIM_HER
		
		#FertilityGod
		FertilityGodName = paganism_fertility_god_name
		FertilityGodNamePossessive = paganism_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_SHE
		FertilityGodHerHis = CHARACTER_HERHIS_HER
		FertilityGodHerHim = CHARACTER_HERHIM_HER

		#WealthGod
		WealthGodName = paganism_wealth_god_name
		WealthGodNamePossessive = paganism_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_SHE
		WealthGodHerHis = CHARACTER_HERHIS_HER
		WealthGodHerHim = CHARACTER_HERHIM_HER

		#HouseholdGod
		HouseholdGodName = paganism_household_god_name
		HouseholdGodNamePossessive = paganism_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_SHE
		HouseholdGodHerHis = CHARACTER_HERHIS_HER
		HouseholdGodHerHim = CHARACTER_HERHIM_HER

		#FateGod
		FateGodName = paganism_fate_god_name
		FateGodNamePossessive = paganism_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_IT
		FateGodHerHis = CHARACTER_HERHIS_ITS
		FateGodHerHim = CHARACTER_HERHIM_IT

		#KnowledgeGod
		KnowledgeGodName = paganism_knowledge_god_name
		KnowledgeGodNamePossessive = paganism_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_SHE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HER
		KnowledgeGodHerHim = CHARACTER_HERHIM_HER

		#WarGod
		WarGodName = paganism_war_god_name
		WarGodNamePossessive = paganism_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_SHE
		WarGodHerHis = CHARACTER_HERHIS_HER
		WarGodHerHim = CHARACTER_HERHIM_HER

		#TricksterGod
		TricksterGodName = paganism_trickster_god_name
		TricksterGodNamePossessive = paganism_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = paganism_night_god_name
		NightGodNamePossessive = paganism_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_HE
		NightGodHerHis = CHARACTER_HERHIS_HIS
		NightGodHerHim = CHARACTER_HERHIM_HIM

		#WaterGod
		WaterGodName = paganism_water_god_name
		WaterGodNamePossessive = paganism_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_SHE
		WaterGodHerHis = CHARACTER_HERHIS_HER
		WaterGodHerHim = CHARACTER_HERHIM_HER


		PantheonTerm = religion_the_gods
		PantheonTerm2 = religion_the_gods_2
		PantheonTerm3 = religion_the_gods_3
		PantheonTermHasHave = pantheon_term_have
		GoodGodNames = { skaldhyrric_high_god_name skaldhyrric_high_god_name_alternate paganism_good_god_father_sky paganism_good_god_ancestors }
		DevilName = skaldhyrric_devil_name
		DevilNamePossessive = skaldhyrric_devil_name_possessive
		DevilSheHe = skaldhyrric_devil_shehe
		DevilHerHis = skaldhyrric_devil_shehe
		DevilHerselfHimself = skaldhyrric_devil_herselfhimself
		EvilGodNames = { skaldhyrric_devil_name paganism_evil_god_decay }
		HouseOfWorship = skaldhyrric_house_of_worship
		HouseOfWorship2 = skaldhyrric_house_of_worship
		HouseOfWorship3 = skaldhyrric_house_of_worship
		HouseOfWorshipPlural = skaldhyrric_house_of_worship_plural
		ReligiousSymbol = skaldhyrric_religious_symbol
		ReligiousSymbol2 = skaldhyrric_religious_symbol
		ReligiousSymbol3 = skaldhyrric_religious_symbol
		ReligiousText = skaldhyrric_religious_text
		ReligiousText2 = skaldhyrric_religious_text
		ReligiousText3 = skaldhyrric_religious_text
		ReligiousHeadName = skaldhyrric_religious_head_title
		ReligiousHeadTitleName = skaldhyrric_religious_head_title_name
		DevoteeMale = skaldhyrric_devotee_male
		DevoteeMalePlural = skaldhyrric_devotee_male_plural
		DevoteeFemale = skaldhyrric_devotee_female
		DevoteeFemalePlural = skaldhyrric_devotee_female_plural
		DevoteeNeuter = skaldhyrric_devotee_neuter
		DevoteeNeuterPlural = skaldhyrric_devotee_neuter_plural
		PriestMale = skaldhyrric_priest
		PriestMalePlural = skaldhyrric_priest_plural
		PriestFemale = skaldhyrric_priest
		PriestFemalePlural = skaldhyrric_priest_plural
		PriestNeuter = skaldhyrric_priest
		PriestNeuterPlural = skaldhyrric_priest_plural
		AltPriestTermPlural = skaldhyrric_priest_term_plural
		BishopMale = skaldhyrric_bishop
		BishopMalePlural = skaldhyrric_bishop_plural
		BishopFemale = skaldhyrric_bishop
		BishopFemalePlural = skaldhyrric_bishop_plural
		BishopNeuter = skaldhyrric_bishop
		BishopNeuterPlural = skaldhyrric_bishop_plural
		DivineRealm = skaldhyrric_divine_realm
		DivineRealm2 = skaldhyrric_divine_realm
		DivineRealm3 = skaldhyrric_divine_realm
		PositiveAfterLife = skaldhyrric_positive_afterlife
		PositiveAfterLife2 = skaldhyrric_positive_afterlife
		PositiveAfterLife3 = skaldhyrric_positive_afterlife
		NegativeAfterLife = skaldhyrric_negative_afterlife
		NegativeAfterLife2 = skaldhyrric_negative_afterlife
		NegativeAfterLife3 = skaldhyrric_negative_afterlife
		DeathDeityName = skaldhyrric_death_name
		DeathDeityNamePossessive = skaldhyrric_death_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		DeathDeityHerHim = CHARACTER_HERHIM_HIM

		GHWName = ghw_purification
		GHWNamePlural = ghw_purifications
	}

	faiths = {
		skaldhyrric_faith = {
			color = { 255 201 201 }
			icon = skaldhyrric_faith
			religious_head = d_skaldskola

			holy_site = skaldol
			holy_site = coldmarket
			holy_site = algrar
			holy_site = jotunhamr
			holy_site = bal_vroren
			#holy_site = drekiriki
			#holy_site = westport

			doctrine = tenet_ancestor_worship	
			doctrine = tenet_skaldic_tales
			doctrine = tenet_warmonger	
		}
		old_gerudian_faith = { #Unfinished, going to be replaced?
			color = { 186 122 86 }
			icon = the_dame
			
			#Temporary, so game doesn't crash when going on pilgrimage
			holy_site = skaldol
			holy_site = coldmarket
			holy_site = algrar
			holy_site = jotunhamr
			holy_site = bal_vroren
			
			doctrine = tenet_ancestor_worship	
			doctrine = tenet_gruesome_festivals
			doctrine = tenet_warmonger
			
			localization  = {
				HighGodName = old_gerudian_faith_high_god_name
				HighGodName2 = old_gerudian_faith_high_god_name
				HighGodNamePossessive = old_gerudian_faith_high_god_name_possessive
			}
		}
		taric_faith = { #Unfinished
			color = { 89 53 53 }
			icon = the_dame
			reformed_icon = the_dame
			
			#Temporary, so game doesn't crash when going on pilgrimage
			holy_site = skaldol
			holy_site = coldmarket
			holy_site = algrar
			holy_site = jotunhamr
			holy_site = bal_vroren
			
			doctrine = tenet_sanctity_of_nature
			doctrine = tenet_ancestor_worship
			doctrine = tenet_communal_identity
			
			doctrine = unreformed_faith_doctrine
			
			localization  = {
				HighGodName = taric_faith_high_god_name
				HighGodName2 = taric_faith_high_god_name
				HighGodNamePossessive = taric_faith_high_god_name_possessive
			}
		}
	}
}
