﻿county_has_quest = {
	trigger_if = {
		limit = { tier < tier_county }
		county = {
			has_quest = yes
		}
	}
	trigger_else_if = {
		limit = { tier = tier_county }
		has_quest = yes
	}
	trigger_else = {
		always = no # Only counties have quests
	}
}

has_quest = {
	# TODO - maybe we use a flag rather than checking for every possible modifier if we end up making a lot of quests
	custom_tooltip = {
		text = HAS_ACTIVE_QUEST_TRIGGER
		OR = {
			has_county_modifier = goblins_infestation_modifier
		}
	}
}

county_can_spawn_quest = {
	# TODO
}