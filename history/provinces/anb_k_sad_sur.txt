#k_ekluzagnu
##d_zansap
###c_zansap
596 = {		#Zansap

    # Misc
    culture = barsibu
    religion = cult_of_the_stone
	holding = castle_holding

    # History
}

6561 = {		#

    # Misc
    culture = barsibu
    religion = cult_of_the_stone
	holding = none
    # History
}

6562 = {		#

    # Misc
    culture = barsibu
    religion = cult_of_the_stone
	holding = none
    # History
}

6563 = {		#

    # Misc
    culture = barsibu
    religion = cult_of_the_stone
	holding = none
    # History
}

6564 = {		#

    # Misc
    culture = barsibu
    religion = cult_of_the_stone
	holding = none
    # History
}

###c_surib
598 = {		#Surib

    # Misc
    culture = barsibu
    religion = cult_of_the_stone
	holding = castle_holding

    # History
}

6564 = {		#

    # Misc
    culture = barsibu
    religion = cult_of_the_stone
	holding = none
    # History
}

6565 = {		#

    # Misc
    culture = barsibu
    religion = cult_of_the_stone
	holding = none
    # History
}

6566 = {		#

    # Misc
    culture = barsibu
    religion = cult_of_the_stone
	holding = none
    # History
}

##d_nabasih
###c_betlibar
606 = {		#Betlibar

    # Misc
    culture = zanite
    religion = cult_of_the_stone
	holding = castle_holding

    # History
}

6599 = {		#

    # Misc
    culture = zanite
    religion = cult_of_the_stone
	holding = none
    # History
}

6600 = {		#

    # Misc
    culture = zanite
    religion = cult_of_the_stone
	holding = none
    # History
}

###c_zagnuhar
605 = {		#Zagnuhar

    # Misc
    culture = zanite
    religion = cult_of_the_stone
	holding = castle_holding

    # History
}

6591 = {		#

    # Misc
    culture = zanite
    religion = cult_of_the_stone
	holding = none
    # History
}

6592 = {		#

    # Misc
    culture = zanite
    religion = cult_of_the_stone
	holding = none
    # History
}

###c_elisumu
690 = {		#Elisumu

    # Misc
    culture = zanite
    religion = cult_of_the_stone
	holding = castle_holding

    # History
}

6593 = {		#

    # Misc
    culture = zanite
    religion = cult_of_the_stone
	holding = none
    # History
}

6594 = {		#

    # Misc
    culture = zanite
    religion = cult_of_the_stone
	holding = none
    # History
}

##d_sad_sur
###c_sad_sur
682 = {		#Sad Sur

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

6679 = {		#

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none
    # History
}

6680 = {		#

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none
    # History
}

###c_sihbaga
680 = {		#Sihbaga

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = tribal_holding

    # History
}

6681 = {		#

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = none
    # History
}

6682 = {		#

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = none
    # History
}

###c_rakutmu
679 = {		#Rakutmu

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = tribal_holding

    # History
}

6683 = {		#

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = none
    # History
}

##d_agsilsu
###c_arfajazan
597 = {		#Arfajazan

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = tribal_holding

    # History
}

6684 = {		#

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = none
    # History
}

6685 = {		#

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = none
    # History
}

###c_umulu
685 = {		#Umulu

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

6686 = {		#

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none
    # History
}

6687 = {		#

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none
    # History
}

###c_elum_szel_bazi
683 = {		#Elum-szel-bazi

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

6688 = {		#

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none
    # History
}

##d_ekluzagnu
###c_ekluzagnu
688 = {		#Ekluzagnu

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = tribal_holding

    # History
	1000.1.1 = {
		special_building_slot = ekluzagnu_01
		special_building = ekluzagnu_01
	}
}

6689 = {		#

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = none
    # History
}

6690 = {		#

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = none
    # History
}

6691 = {		#

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = church_holding
    # History
}

###c_sad_uras
689 = {		#Sad Uras

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = tribal_holding

    # History
	1000.1.1 = {
		special_building_slot = sad_uras_mines_01
		special_building = sad_uras_mines_01
	}
}

6692 = {		#

    # Misc
    culture = sadnatu
    religion = cult_of_the_stone
	holding = none
    # History
}

##d_ardutibad
###c_anzakalis
687 = {		#Anzakalis

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

6693 = {		#

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none
    # History
}

###c_harranbar
691 = {		#Harranbar

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

6694 = {		#

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none
    # History
}

##d_tumerub
###c_kesudagka
693 = {		#Kesudagka

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

6695 = {		#

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none
    # History
}

###c_mibunasru
686 = {		#Mibunasru

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

6696 = {		#

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none
    # History
}

6697 = {		#

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none
    # History
}

###c_ardubar
692 = {		#Ardubar

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

6698 = {		#

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none
    # History
}
