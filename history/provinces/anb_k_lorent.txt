#k_lorent
##d_lorentaine
###c_lorentaine
67 = {		#Lorentainé

    # Misc
    culture =  lorentish
    religion = whiterose_court
    holding = castle_holding

    # History
}
1021 = {

    # Misc
    holding = none

    # History

}
1022 = {

    # Misc
    holding = church_holding

    # History

}
1023 = {

    # Misc
    holding = castle_holding

    # History

}
1040 = {

    # Misc
    holding = city_holding

    # History
    effect = { add_province_modifier = ruby_dwarvish_city }

}

###c_rewanfork
69 = {		#Rewanfork

    # Misc
    culture =  lorenti
    religion = eidoueni
    holding = castle_holding

    # History
}
1015 = {

    # Misc
    holding = none

    # History

}
1014 = {

    # Misc
    holding = city_holding

    # History

}

###c_ionnidar
70 = {		#Ionnidar

    # Misc
    culture =  lorentish
    religion = whiterose_court
    holding = castle_holding

    # History
}
1024 = {

    # Misc
    holding = none

    # History

}
1025 = {

    # Misc
    holding = none

    # History

}
1096 = {

    # Misc
    holding = city_holding

    # History

}

##d_rosefield
###c_rosefield
133 = {		#Rosefield

    # Misc
    culture =  lorenti
    religion = whiterose_court
    holding = castle_holding

    # History
}
71 = {		#Ruby Pass

    # Misc
    holding = castle_holding

    # History
}
1174 = {

    # Misc
    holding = none

    # History

}
1175 = {

    # Misc
    holding = none

    # History

}

###c_lasean
158 = {		#Lasean

    # Misc
    culture =  lorenti
    religion = whiterose_court
    holding = castle_holding

    # History
}
1176 = {

    # Misc
    holding = none

    # History

}
1177 = {

    # Misc
    holding = none

    # History

}
1178 = {

    # Misc
    holding = city_holding

    # History

}

###c_ar_esta
152 = {		#Ar Esta

    # Misc
    culture =  lorenti
    religion = eidoueni
    holding = castle_holding

    # History
}
1179 = {

    # Misc
    holding = none

    # History

}
1180 = {

    # Misc
    holding = city_holding

    # History

}
1181 = {

    # Misc
    holding = church_holding

    # History

}
1182 = {

    # Misc
    holding = none

    # History

}

##d_lorenith
###c_lorenith
61 = {		#Lorenith

    # Misc
    culture =  lorenti
    religion = whiterose_court
    holding = castle_holding

    # History
}
1064 = {

    # Misc
    holding = church_holding

    # History

}
1013 = {

    # Misc
    holding = none

    # History

}

###c_redfort
68 = {		#Redfort

    # Misc
    culture =  lorenti
    religion = whiterose_court
    holding = castle_holding

    # History
}
1018 = {

    # Misc
    holding = none

    # History

}
1016 = {

    # Misc
    holding = castle_holding

    # History

}

##d_ainethan
###c_ainethan
72 = {		#Ainethan

    # Misc
    culture =  lorentish
    religion = whiterose_court
    holding = castle_holding

    # History
}
1020 = {

    # Misc
    holding = city_holding

    # History

}
1095 = {

    # Misc
    holding = none

    # History

}

###c_oldport
38 = {		#Oldport

    # Misc
    culture =  lorenti
    religion = whiterose_court
    holding = city_holding

    # History
}
1017 = {

    # Misc
    holding = none

    # History

}
1075 = {

    # Misc
    holding = church_holding

    # History

}
1093 = {

    # Misc
    holding = city_holding

    # History

}

###c_casthil
73 = {		#Casthil

    # Misc
    culture =  lorentish
    religion = whiterose_court
    holding = castle_holding

    # History
}
1019 = {

    # Misc
    holding = none

    # History

}
1092 = {

    # Misc
    holding = city_holding

    # History

}

##d_redglades
###c_ioriellen
115 = {		#Iorellen

    # Misc
    culture =  moon_elvish
    religion = whiterose_court #Placeholder
    holding = castle_holding

    # History
}
1168 = {

    # Misc
    holding = none

    # History

}
1169 = {

    # Misc
    holding = city_holding

    # History

}

###c_springglade
117 = {		#Springlade

    # Misc
    culture =  moon_elvish
    religion = whiterose_court #Placeholder
    holding = castle_holding

    # History
}
1170 = {

    # Misc
    holding = city_holding

    # History

}
1171 = {

    # Misc
    holding = none

    # History

}

###c_summerglade
118 = {		#Summerglade

    # Misc
    culture =  moon_elvish
    religion = whiterose_court #Placeholder
    holding = castle_holding

    # History
}
1172 = {

    # Misc
    holding = none

    # History

}

###c_winterglade
120 = {		#Winterglade

    # Misc
    culture =  moon_elvish
    religion = whiterose_court #Placeholder
    holding = castle_holding

    # History
}
1167 = {

    # Misc
    holding = none

    # History

}

###c_autumnglade
116 = {		#Autumnglade

    # Misc
    culture =  moon_elvish
    religion = whiterose_court #Placeholder
    holding = castle_holding

    # History
}
1165 = {

    # Misc
    holding = none

    # History

}
1166 = {

    # Misc
    holding = church_holding

    # History

}

##d_rewanwood
###c_rewanwood
119 = {		#Rewanwood

    # Misc
    culture =  lorenti
    religion = whiterose_court
    holding = castle_holding

    # History
}
1161 = {

    # Misc
    holding = none

    # History

}

1162 = {

    # Misc
    holding = city_holding

    # History

}

###c_gladegate
77 = {		#Gladegate

    # Misc
    culture =  moon_elvish
    religion = whiterose_court
    holding = castle_holding

    # History
}
1163 = {

    # Misc
    holding = none

    # History

}
1164 = {

    # Misc
    holding = none

    # History

}

###c_wesmar
87 = {		#Wesmar

    # Misc
    culture =  lorenti
    religion = whiterose_court
    holding = castle_holding

    # History

}
1158 = {

    # Misc
    holding = church_holding

    # History

}
1159 = {

    # Misc
    holding = city_holding

    # History

}
1160 = {

    # Misc
    holding = none

    # History

}

##d_upper_bloodwine
###c_kyliande
74 = {		#Kyliande

    # Misc
    culture = lorentish
    religion = whiterose_court
    holding = castle_holding

    # History
}
1026 = {

    # Misc
    holding = church_holding

    # History

}
1027 = {

    # Misc
    holding = none

    # History

}
1065 = {

    # Misc
    holding = city_holding

    # History

}

###c_palevine
114 = {		#Palevine

    # Misc
    culture = lorenti
    religion = whiterose_court
    holding = castle_holding

    # History
}
1028 = {

    # Misc
    holding = none

    # History

}
1029 = {

    # Misc
    holding = church_holding

    # History

}
1066 = {

    # Misc
    holding = city_holding

    # History

}
1068 = {

    # Misc
    holding = none

    # History

}

###c_greenfield
108 = {		#Greenfield

    # Misc
    culture = lorenti
    religion = house_of_minara
    holding = castle_holding

    # History
}
1031 = {

    # Misc
    holding = city_holding

    # History

}
1032 = {

    # Misc
    holding = none

    # History

}
1067 = {

    # Misc
    holding = none

    # History

}

###c_nurionn
95 = {		#Nurionn

    # Misc
    culture = lorentish
    religion = house_of_minara
    holding = castle_holding

    # History
}
1030 = {

    # Misc
    holding = none

    # History

}
1072 = {

    # Misc
    holding = none

    # History

}

##d_lower_bloodwine
###c_lower_bloodwine
79 = {		#Lower Bloodwine

    # Misc
    culture =  lorenti
    religion = house_of_minara
    holding = castle_holding

    # History
}
1034 = {

    # Misc
    holding = none

    # History

}
1033 = {

    # Misc
    holding = city_holding

    # History

}
1069 = {

    # Misc
    holding = none

    # History

}
1097 = {	#East Wagonwood

    # Misc
    holding = church_holding

    # History

}

###c_minar
97 = {		#Minar

    # Misc
    culture = lorenti
    religion = house_of_minara
    holding = church_holding

    # History
}
1036 = {

    # Misc
    holding = castle_holding

    # History

}

###c_pircost
80 = {		#Pircost

    # Misc
    culture = lorenti
    religion = house_of_minara
    holding = castle_holding

    # History
}
1035 = {

    # Misc
    holding = none

    # History

}
1071 = {

    # Misc
    holding = city_holding

    # History

}

###c_foxalley
81 = {		#Foxalley

    # Misc
    culture = lorenti
    religion = house_of_minara
    holding = castle_holding

    # History
}
1038 = {

    # Misc
    holding = none

    # History

}
1070 = {

    # Misc
    holding = church_holding

    # History

}

###c_wineport
101 = {		#Wineport

    # Misc
    culture = lorenti
    religion = house_of_minara
    holding = city_holding

    # History
}
1037 = {

    # Misc
    holding = castle_holding

    # History

}
1073 = {

    # Misc
    holding = none

    # History

}
