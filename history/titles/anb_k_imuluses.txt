k_imuluses = {
	1007.9.3 = {
		liege = e_bulwar
		holder = amarienzuir0001 # Amarien Amarienzuir
	}
}

d_imuluses = {
	1007.9.3 = {
		holder = amarienzuir0001 # Amarien Amarienzuir
	}
}

c_eonkavan = {
	1000.1.1 = { change_development_level = 12 }
	1007.9.3 = {
		holder = amarienzuir0001 # Amarien Amarienzuir
	}
}

c_birsartansbar = {
	1000.1.1 = { change_development_level = 16 }
}

c_tunkalem = {
	1000.1.1 = { change_development_level = 10 }
}

c_kaboustraz = {
	1000.1.1 = { change_development_level = 10 }
}

c_sad_kuz = {
	1000.1.1 = { change_development_level = 12 }
}

c_kuztanuz = {
	1000.1.1 = { change_development_level = 14 }
}

c_tuklum_elum = {
	1000.1.1 = { change_development_level = 9 }
}

c_barzilsad = {
	1000.1.1 = { change_development_level = 12 }
}