﻿namespace = anb_decision_major_events

#I have united Castellyr
anb_decision_major_events.0001 = {
	type = character_event
	title = anb_decision_major_events.0001.t
	desc = anb_decision_major_events.0001.desc
	theme = realm
	left_portrait = {
		character = scope:castellyr_former
		animation = personality_bold
	}

	immediate = {
		play_music_cue = "mx_cue_positive_effect"
		form_castellyr_decision_effects = yes
	}
	
	option = {
		name = anb_decision_major_events.0001.a

		give_nickname = nick_the_motherfather_of_castellyr
	}
}

#Someone united Castellyr
anb_decision_major_events.0002 = {
	type = character_event
	title = anb_decision_major_events.0001.t
	desc = {
		desc = anb_decision_major_events.0002.start.desc
		first_valid = {
			triggered_desc = {
				trigger = {
					has_RelationToMe_relation = { CHARACTER = scope:castellyr_former }
				}
				desc = anb_decision_major_events.0002.relation_former.desc
			}
			desc = anb_decision_major_events.0002.former.desc
		}	
		desc = anb_decision_major_events.0002.end.desc		
	}
	theme = realm
	left_portrait = {
		character = scope:castellyr_former
		animation = personality_bold
	}
	
	option = {
		name = name_i_see
	}
}

#I have united the reach
anb_decision_major_events.0003 = {
	type = character_event
	title = anb_decision_major_events.0003.t
	desc = anb_decision_major_events.0003.desc
	theme = realm
	left_portrait = {
		character = scope:the_reach_former
		animation = personality_bold
	}

	immediate = {
		play_music_cue = "mx_cue_positive_effect"
		unite_the_reach_decision_effect = yes
	}
	
	option = {
		name = anb_decision_major_events.0003.a

		give_nickname = nick_the_motherfather_of_the_reach
	}
}

#Someone united the reach!
anb_decision_major_events.0004 = {
	type = character_event
	title = anb_decision_major_events.0004.t
	desc = {
		desc = anb_decision_major_events.0004.start.desc
		first_valid = {
			triggered_desc = {
				trigger = {
					has_RelationToMe_relation = { CHARACTER = scope:the_reach_former }
				}
				desc = anb_decision_major_events.0004.relation_former.desc
			}
			desc = anb_decision_major_events.0004.former.desc
		}	
		desc = anb_decision_major_events.0004.end.desc		
	}
	theme = realm
	left_portrait = {
		character = scope:the_reach_former
		animation = personality_bold
	}
	
	option = {
		name = name_i_see
	}
}


#I have united Verne
anb_decision_major_events.0005 = { 
	type = character_event
	title = anb_decision_major_events.0005.t
	desc = anb_decision_major_events.0005.desc
	theme = realm
	left_portrait = {
		character = scope:verne_uniter
		animation = personality_bold
	}

	immediate = {
		play_music_cue = "mx_cue_positive_effect"
		unite_the_vernman_kingdoms_decision_effects = yes
	}
	
	option = {
		name = anb_decision_major_events.0005.a

		give_nickname = nick_the_motherfather_of_verne
	}
}

#Someone united Verne
anb_decision_major_events.0006 = { 
	type = character_event
	title = anb_decision_major_events.0005.t
	desc = {
		desc = anb_decision_major_events.0006.start.desc
		first_valid = {
			triggered_desc = {
				trigger = {
					has_RelationToMe_relation = { CHARACTER = scope:verne_uniter }
				}
				desc = anb_decision_major_events.0006.relation_former.desc
			}
			desc = anb_decision_major_events.0006.former.desc
		}	
		desc = anb_decision_major_events.0006.end.desc		
	}
	theme = realm
	left_portrait = {
		character = scope:verne_uniter
		animation = personality_bold
	}
	
	option = {
		name = name_i_see
	}
}

anb_decision_major_events.0100 = {
	type = character_event
	title = anb_decision_major_events.0100.t
	desc = 	anb_decision_major_events.0100.desc
	left_portrait = root
	theme = culture_change
	override_background = { reference = wilderness_scope }
	immediate = {
		every_vassal = {
			limit = {
				OR = {
					culture:humacfelder
					culture:trystanite
					culture:marcher
					culture:castanorian
				}
			}
			add_opinion = {
				modifier = increased_opinion
				target = root
			}
		}
	}
	option = {
		name = anb_decision_major_events.0100.a # Remain the same
		ai_chance = {
			base = 100
		}
	}
	option = {
		name = anb_decision_major_events.0100.b # Swap Hill Dwellers
		culture:humacfelder = {
			remove_culture_tradition = tradition_hill_dwellers
			add_culture_tradition = tradition_agrarian
		}
		ai_chance = {
			base = 25
		}
		
	}
	option = {
		name = anb_decision_major_events.0100.c # Swap Collective Lands
		culture:humacfelder = {
			remove_culture_tradition = tradition_collective_lands
			add_culture_tradition = tradition_formation_fighting
		}
		ai_chance = {
			base = 25
		}
	}
	option = {
		name = anb_decision_major_events.0100.d # Swap Fervent Temple Buliders
		culture:humacfelder = {
			remove_culture_tradition = tradition_fervent_temple_builders
			add_culture_tradition = tradition_castle_keepers
		}
		ai_chance = {
			base = 25
		}
	}
}