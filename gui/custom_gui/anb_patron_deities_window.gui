types PatronWindow {
	type faith_window_patrons_tabs = vbox {
		name = "tab_anb_patrons"
		visible = "[GetVariableSystem.HasValue( 'faith_view_tabs', 'anb_patrons' )]"
		layoutpolicy_horizontal = expanding
		layoutpolicy_vertical = expanding
		datacontext = "[GetIllustration( 'religion_holding' )]"

		blockoverride "scrollbox_background" {}
		blockoverride "scrollbox_background_fade" {}

		state = {
			name = _show
			using = Animation_FadeIn_Quick
		}

		state = {
			name = _hide
			alpha = 0
		}

		background = {
			alpha = 0.5
			fittype = end
			texture = "[Illustration.GetTexture( FaithWindow.GetFaith.MakeScope )]"

			modify_texture = {
				mirror = vertical
				blend_mode = alphamultiply
				texture = "gfx/interface/component_masks/mask_fade_vertical.dds"
			}
		}
		patron_list_builder = {}
	}

	type anb_patron_tab = button_tab {
		layoutpolicy_horizontal = expanding
		visible = "[GetScriptedGui('anb_faith_has_patron_gods').IsValid( GuiScope.SetRoot( GetPlayer.MakeScope ).AddScope( 'window_faith', FaithWindow.GetFaith.MakeScope ).End ) ]"
		onclick = "[GetVariableSystem.Set( 'faith_view_tabs', 'anb_patrons' )]"
		down = "[GetVariableSystem.HasValue( 'faith_view_tabs', 'anb_patrons' )]"
		text = "anb_patrons"
		default_format = "#low"

		using = tooltip_below
	}
	type patron_list_builder = vbox {
		layoutpolicy_horizontal = expanding
		layoutpolicy_vertical = expanding
		scrollbox = {
			layoutpolicy_horizontal = expanding
			layoutpolicy_vertical = expanding
			blockoverride "scrollbox_content"
			{
				vbox = {
					spacing = 10
					margin_bottom = 30
					margin_top = 5
					layoutpolicy_horizontal = expanding
					datamodel = "[FaithWindow.GetFaith.MakeScope.GetList('patron_gods')]"
					item = {
						vbox = {
							vbox = {
								margin_bottom = 8
								margin_top = 0
								button_standard = {
									enabled = "[GetScriptedGui('anb_can_pick_patron_gods').IsValid( GuiScope.SetRoot( GetPlayer.MakeScope ).AddScope( 'window_faith', FaithWindow.GetFaith.MakeScope ).End ) ]"
									onclick = "[GetScriptedGui('anb_can_pick_patron_gods').Execute( GuiScope.SetRoot( GetPlayer.MakeScope ).AddScope( 'patron', MakeScopeFlag(Scope.GetFlagName)).AddScope('patron_global', GetGlobalVariable(Scope.GetFlagName).Char.MakeScope ).AddScope( 'window_faith', FaithWindow.GetFaith.MakeScope ).End ) ]"
									tooltip = "[GetScriptedGui('anb_can_pick_patron_gods_tt').BuildTooltip( GuiScope.SetRoot( GetPlayer.MakeScope ).AddScope( 'window_faith', FaithWindow.GetFaith.MakeScope ).End ) ]"
									size = { 720 110 }
									background = {
										visible = "[GetScriptedGui('anb_has_patron').IsValid( GuiScope.SetRoot( GetPlayer.MakeScope ).AddScope( 'patron', MakeScopeFlag(Scope.GetFlagName)).End ) ]"
										using = Background_Area_Light
										color = { 0.13 0.13 0.13 0.85 }
									}
									hbox = {
										maximumsize = { 750 30 }
										visible = "[GetScriptedGui('anb_can_apot_god').IsValid( GuiScope.SetRoot( GetPlayer.MakeScope ).AddScope( 'patron', MakeScopeFlag(Scope.GetFlagName)).End ) ]"
										text_single = {
											layoutpolicy_horizontal = expanding
											name = "patron_deity_name"
											text = patron_apotheosized_name
											align = left
											margin = { 110 }
											autoresize = yes
											default_format = "#high"
											background = {
												using = Text_Label_Background
												alpha = 0.66
												margin = { 0 3 }
												margin_bottom = 5
											}
											hbox = {
												maximumsize = { 550 30 }
												margin_top = 25
												text_multi = {
													layoutpolicy_horizontal = growing
													name = "patron_deity_desc"
													text = patron_apotheosized_desc
													align = left
													margin = { 110 }
													maximumsize = { 550 80 }
													autoresize = yes
													default_format = "#low;italic"
												}
											}
										}
									}
									hbox = {
										maximumsize = { 750 30 }
										visible = "[Not(GetScriptedGui('anb_can_apot_god').IsValid( GuiScope.SetRoot( GetPlayer.MakeScope ).AddScope( 'patron', MakeScopeFlag(Scope.GetFlagName)).End ))]"
										text_single = {
											layoutpolicy_horizontal = expanding
											name = "patron_deity_name"
											text = "[Localize(Concatenate('patron_',Scope.GetFlagName))]"
											align = left
											margin = { 110 }
											autoresize = yes
											default_format = "#high"
											background = {
												using = Text_Label_Background
												alpha = 0.66
												margin = { 0 3 }
												margin_bottom = 5
											}
											hbox = {
												maximumsize = { 550 30 }
												margin_top = 25
												text_multi = {
													layoutpolicy_horizontal = growing
													name = "patron_deity_desc"
													text = "[Localize(Concatenate(Concatenate('patron_',Scope.GetFlagName),'_desc'))]"
													align = left
													margin = { 110 }
													maximumsize = { 550 80 }
													autoresize = yes
													default_format = "#low;italic"
												}
											}
										}
									}
									hbox = {
										name = "icon_parent"
										layoutpolicy_horizontal = expanding
										layoutpolicy_vertical = expanding
										expand = {}
										hbox = {
											name = "icon"
											layoutpolicy_horizontal = expanding
											highlight_icon = {
												texture = "[GetCultureInnovationType(Concatenate('patron_', Scope.GetFlagName)).GetIcon]"
												visible = "[Not(GetScriptedGui('anb_can_apot_god').IsValid( GuiScope.SetRoot( GetPlayer.MakeScope ).AddScope( 'patron', MakeScopeFlag(Scope.GetFlagName)).End )) ]"
												size = { 100 100 }
											}
											patron_portrait_head_small_legend = {
												datacontext = "[GetGlobalVariable(Scope.GetFlagName).Char]"
												visible = "[GetScriptedGui('anb_can_apot_god').IsValid( GuiScope.SetRoot( GetPlayer.MakeScope ).AddScope( 'patron', MakeScopeFlag(Scope.GetFlagName)).End ) ]"
												size = { 100 100 }
												blockoverride "coa" {}
												blockoverride highlight_icon {}
											}
											divider_light = {
												layoutpolicy_vertical = expanding
											}
											expand = {}
										}
										vbox = {
											name = "patron_modifiers"
											margin_right = 10
											text_multi = {
												fontsize = 13
												visible = "[Not(GetScriptedGui('anb_can_apot_god').IsValid( GuiScope.SetRoot( GetPlayer.MakeScope ).AddScope( 'patron', MakeScopeFlag(Scope.GetFlagName)).End )) ]"
												text = "[GetModifier(Concatenate('patron_', Concatenate(Scope.GetFlagName,'_modifier'))).GetDescWithEffects]"
												maximumsize = { 270 270 }
												autoresize = yes
												align = right
											}
											text_multi = {
												fontsize = 13
												visible = "[GetScriptedGui('anb_can_apot_god').IsValid( GuiScope.SetRoot( GetPlayer.MakeScope ).AddScope( 'patron', MakeScopeFlag(Scope.GetFlagName)).End ) ]"
												text = "[GetModifier(Concatenate(GetGlobalVariable(Concatenate(Scope.GetFlagName,'_portfolio_category')).GetFlagName,'_patron_apot_modifier')).GetDescWithEffects]"
												maximumsize = { 270 270 }
												autoresize = yes
												align = right
											}
											expand = {}
										}
									}
								}
							}
							divider_light = {
								layoutpolicy_horizontal = expanding
							}
						}
					}
				}
			}
		}
	}
	type patron_portrait_head_small_legend = widget {
		size = { 95 120 }

		portrait_button = {
			parentanchor = bottom|right
			size = { 90 120 }
			using = portrait_base

			shaderfile = "gfx/FX/gui_painterly_portrait.shader"
			pop_out = yes # Painterly shader override for `pop_out` property, it now means "add outline"
			pop_out_v = 1.2 # Painterly shader override for `pop_out_v` property, it now means "outline width"
			grayscale = no # Dead people should be painted non-gray

			block "portrait_texture"
			{
				portrait_texture = "[Character.GetPortrait('environment_head', 'camera_head', 'idle', PdxGetWidgetScreenSize(PdxGuiWidget.Self))]"
			}
			mask = "gfx/portraits/portrait_mask_head.dds"
			effectname = "NoHighlight"
		}
	}
}
# effect faith = { add_to_variable_list = { name = patron_gods target = flag:munas } }